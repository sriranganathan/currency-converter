## Currency Converter

As the name suggests this is a web app which is powered by CURRENCY LAYER API. As of now this app can convert 168 currencies.
The app not only gives you the current conversion rates but also the rates of an earlier date (till 01-jan-2015). As the date
goes down the number of currencies supported decreases. In both the history and live tabs search params are recieved. the search param for live include the quantity,from currency,to currency and the graph mode. There are three graph modes,the first produces a graph with rates of 10 previous days , the second with rates of 10 previous weeks and the last with rates of 10 previous months. the param for history includes all params of live and the date, whose conversion rate is to be returned.

##SERVER ROUTES

/- the welcome page. The page provides the user to toggle between two tabs namely live and history where the searches can be made. this page also provides the user to sign up or login.<br>
/liveform - the page provides the user the conversion rate and the graph based on the search query<br>
/historyform - the page provides the user the conversion rate of an earlier date and the graph based on the search query<br>
/currencytable - a html page with all supported currencies and their codes.<br>
/login - produces a form with a email field and a password field to enable the user to login.<br>
/logincheck - gets data from the form present in /login, checks the credentials entered and logs in the ser or throws error messages<br>
/loginview - produces the view containing the previous searches of the user.<br>
/logout - logs the current user out.<br>
/signup - creates a form with various fields required for sign up of the new user.<br>
/registered - gets data from form present in /signup, checks if the email is already registered. if not adds the user. else throws error messages.<br>

##tables

the tables are stored under a database called spiderweb. the database is created if it does not exist already or used if it exists.<br>

1)rates - table stores live rates. every time a query is made, the table is checked for an entry matching the currencies,and the updatedat coloumn is less than an hour. if an entry is found the details are printed. else if the currencies are matching and the updateat is more than an hour, then the row is updated with values fetched from api. if the entry is not found then a row is created with values fetched from api.<br>
2)graphs - the table stores the data for graphs. the conversion rates along with their dates and currencies invloved are stored. when a graph needs to be plotted, this table is checked along with the date and currencies. if found then values are used or else it is fetched from api and stored in the table and used.<br>

if you have searched for USD to INR already and the value is stored in database, and now you search for INR to USD then the data is not fetched again, the existing data is used to improve the efficiency and speed of the app.<br>

3)accounts - the table stores username userid email and password of a user<br>
4)live_users - the live searches made by a registered user is saved in this table. this table consists coloumns of userid and liveid. this acts as pivot table.<br>
5)historical_users - same as live users table. but for historical searches. stores history id and userid.<br>


##BUILD INSTRUCTIONS

download and install laravel based on instructions from http://laravel.com/.<br>
after installing create a new proj by using 'laravel new <proj name>' command in cmd.<br>
copy paste app,public,database,resources,.env,composer.json files from downloaded folder into your proj,<br>
then open cmd, change path to your proj , type 'php artisan serve'. open browser type localhost:8000<br>
enjoy<br>


##modules used

1) bootstrap - used for styling<br>
2) chart.js - the line chart is created using this module.<br>
  the command for creating a new chart is<br>
  labels:'your label array',<br>
			datasets:[<br>
				{<br>
					label: "My First dataset",<br>
					fillColor : "rgba(220,220,220,0.2)",<br>
					strokeColor : "rgba(220,220,220,1)",<br>
					pointColor : "rgba(220,220,220,1)",<br>
					pointStrokeColor : "#fff",<br>
					pointHighlightFill : "#fff",<br><br>
					pointHighlightStroke : "rgba(220,220,220,1)",<br>
					data:'your data array'<br>
				}<br>
