<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',[function () {
   return view('welcome');
}]);

Route::get('/handle-live-form', array( function()
 {
	$data = Input::all();
	return View::make('liveform', $data);
 }));
 Route::get('/handle-history-form', array(function()
 {
	$data = Input::all();
	return View::make('historyform', $data);
 }));
 Route::get('/currency_table',[function () {
   return view('currency_table');
}]);
Route::get('/signup',[function () {
   return view('signup');
}]);

Route::get('/login',[function () {
   return view('login');
}]);
Route::post('/registered',[function () {
$data = Input::all();
   return view('registered',$data);
}]);
Route::post('/logincheck',[function () {
   $data = Input::all();
   return view('logincheck',$data);
}]);
Route::get('/loginview',[function () {
   return view('loginview');
}]);

Route::get('/logout',[function () {
  return view('logout');
}]);




