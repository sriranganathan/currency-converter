<?php
session_start();
?>

<!DOCTYPE html>
	  <html>
	  <head>
	  <title>LOGGING IN</title>
	  <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="boot.css">
	  <script src="jquery.js"></script>
      <script src="boot.js"></script>
 </head>
 <body>
 <?php
$account =  \App\account::where('email','=',$email)
                              ->get();
  if(count($account))
  {
	 
	 if(Hash::check($pass,$account[0]->password))
	 {
		 echo '
		 
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <div class="alert alert-success col-sm-4 col-xs-8 text-center" >logged in as '.$account[0]->name.'</div>
		 </div>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("loginview").' class="btn btn-info col-sm-4 col-xs-8 btn-lg"> click here to view your previous searches</a>
		 </div>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("/").' class="btn btn-info col-sm-4 col-xs-8 btn-lg "> click here to use to the app</a>
		 </div>';
		 $_SESSION["user"]=$account[0]->id;
	 }
	 else
		echo '
         <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <div class="alert alert-danger col-sm-4 col-xs-8 text-center" >WRONG PASSOWRD</div>
		 </div>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("login").' class="btn btn-info col-sm-4 col-xs-8 btn-lg"> click here to Login again</a>
		 </div>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("/").' class="btn btn-info col-sm-4 col-xs-8 btn-lg "> click here to use to the app without an account</a>
		 </div>';
		 
  }
  else
  {
	  echo '
         <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <div class="alert alert-danger col-sm-4 col-xs-8 text-center" >EMAIL NOT REGISTERED</div>
		 </div>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("signup").' class="btn btn-info col-sm-4 col-xs-8 btn-lg"> click here to signup</a>
		 </div>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("login").' class="btn btn-info col-sm-4 col-xs-8 btn-lg"> try with a different email</a>
		 </div>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("/").' class="btn btn-info col-sm-4 col-xs-8 btn-lg "> click here to use to the app without an account</a>
		 </div>';
  }
 ?>
 </body>
 </html>