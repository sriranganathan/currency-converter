<!DOCTYPE html>
	  <html>
	  <head>
	  <title>SIGN-UP</title>
	  <meta charset="utf-8">
	  <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="boot.css">
	  <script src="jquery.js"></script>
      <script src="boot.js"></script>
  <style>
  *
  {
	  box-sizing:border-box;
  }
  
   body
	  {
		background:url("back.jpg") no-repeat center center fixed; 
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
     background-size: cover;  
	  }
   </style>
  </head>
  
  <body>
  <?php
   
  echo '
  <nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand">CURR<span style="color:yellow;">&#8364</span>NCY Converter</a>
    </div>
  </div>
</nav>
  <br>
  <br>
  <br>
   <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <form class="form-horizontal" role="form" method="post" action='.url("registered").'>

    <div class="row">
	<div class="col-sm-5 col-xs-4" ></div>
    <div class="form-group col-sm-2 col-xs-6" >
      <label for="name">Name:</label>
      <input type="text" class="form-control" id="name" name="name" required>
    </div>
	</div>
	 <div class="row">
	<div class="col-sm-5 col-xs-4" ></div>
    <div class="form-group col-sm-2 col-xs-6" >
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" name="email" required>
    </div>
	</div>
	
	<div class="row">
	<div class="col-sm-5 col-xs-4"></div>
    <div class="form-group col-sm-2 col-xs-6">
      <label for="pass1">Password:</label>
      <input type="password" class="form-control" id="pass1" name="pass1" required>
    </div>
	
	</div>
	<div class="row">
	<div class="col-sm-5 col-xs-4"></div>
    <div class="form-group col-sm-2 col-xs-6">
      <label for="pass2">Retype Password:</label>
      <input type="password" class="form-control" id="pass2" name="pass2" required>
    </div>
	</div>
	<div class="row">
	<div class="col-sm-5 col-xs-4"></div>
    <div class="form-group col-sm-2 col-xs-6">
      <input type="button" class="form-control btn-primary" id="register" value="SIGN UP FOR FREE" onclick="validation();">
    </div>
	</div>
	<input type="submit" style="visibility:hidden;" id="submit">
  </form>
  <script>
  function validation()
{   
    var name = document.getElementById("name").value;
	var email = document.getElementById("email").value;
	var pass1 = document.getElementById("pass1").value;
	var pass2 = document.getElementById("pass2").value;
	name = name.trim();
	document.getElementById("name").value=name;
	var errors=0;
	if(pass1.localeCompare(pass2))
	{
		errors++;
		window.alert("passords don\'t match");
	 	document.getElementById("pass1").value="";
		document.getElementById("pass2").value="";
		
	}
	if((!/^[a-zA-Z]*$/.test(name)))
		{    errors++;
		window.alert("Enter a valid name");
		document.getElementById("name").value="";
		document.getElementById("pass1").value="";
		document.getElementById("pass2").value="";
	}
	if(!(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(email)))
		{    errors++;
		window.alert("Enter a valid email");
		document.getElementById("email").value="";
		document.getElementById("pass1").value="";
		document.getElementById("pass2").value="";
	}
	
	
	if(!errors)
		document.getElementById("submit").click();
}
  </script>
  
  
  
  ';
  ?>
  </body>
  </html>