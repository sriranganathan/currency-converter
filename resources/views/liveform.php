<?php
session_start();
?>

<!DOCTYPE html>
	  <html>
	  <head>
	  <title>Live Search</title>
	  <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="boot.css">
	    <script src="jquery.js"></script>
		<script src="chart.js"></script>
  <script src="boot.js"></script>
  <style>
  #quantity,#from,#to,#result,#eq  
  {   
	  display:inline;
	  box-sizing: border-box;
	  width:auto;
	  height:30px;
	  padding-left:10px;
	  padding-right:10px;
	  padding-top:5px;
	  padding-bottom:5px;
	  margin-top:100px;
	  margin-right:10px;
	  margin-left:10px;
	  margin-bottom:30px;
	  border-radius:12px 12px 12px 12px;
	  text-align:center;
	 
  }
  
  </style>
  </head>
  
  <body>
  <?php 
 $values = array($fromcurrency.$tocurrency, $tocurrency.$fromcurrency);
$search = \App\rate::whereIn('currencies', $values)
		  ->where('updated_at','>=',date("Y-m-d h:i:s",strtotime("-1 hour")))
		  ->get();
			
   			
			


  
  if(count($search)==0)
  {
	  $url = "http://apilayer.net/api/live?access_key=36f135772707a7c2b28a902c07369d50&Currencies=".$fromcurrency.",".$tocurrency."&format=1";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $json = curl_exec($ch);
  curl_close($ch);
  $conversionResult = json_decode($json, true);
  $usdfrom=$conversionResult["quotes"]["USD".$fromcurrency];
  $usdto=$conversionResult["quotes"]["USD".$tocurrency];
  if($usdfrom!=0)
  $converted = round($usdto/$usdfrom*$quantity,5);
  else
  $converted=0;
  
$values = array($fromcurrency.$tocurrency, $tocurrency.$fromcurrency);
$result =  \App\rate::whereIn('currencies', $values)
            ->get();
			if(count($result)!=0)
				$search=$result[0];
			else
				$search =  new \App\rate;
				
$search->currencies = $fromcurrency.$tocurrency;
$search->updated_at = strtotime('now');
$search->result =   $converted;
$search->save();
  }
  else
  {
	  
	  $search = \App\rate::whereIn('currencies', $values)
		       ->first();
		
	$converted = $search->result;
	
	
	if(strcmp($fromcurrency.$tocurrency,$search->currencies)!=0)
	{
		if($converted!=0)
		$converted = 1/$converted;
	    else
		$converted=0;
	}
  }
  
  $history =[];
  if($graph==1)
  {
	  for($i=1;$i<10;$i++)
		  $history[$i-1]=date("Y-m-d",strtotime("-".$i."days"));
		  
  }
  if($graph==2)
  {
	  for($i=1;$i<10;$i++)
		  $history[$i-1]=date("Y-m-d",strtotime("-".$i."weeks"));
		  
  }
  if($graph==3)
  {
	  for($i=1;$i<10;$i++)
		  $history[$i-1]=date("Y-m-d",strtotime("-".$i."months"));
		  
  }
  
  
  
  for($i=0;$i<4;$i++)
  {
	  $temp = $history[$i];
	  $history[$i]=$history[8-$i];
	  $history[8-$i]=$temp;
  }
  $history[9]=date("Y-m-d",strtotime("now"));
  $graphrates=[];
  
  for($i=0;$i<9;$i++)
  {
     $values = array($fromcurrency.$tocurrency, $tocurrency.$fromcurrency);
  $search = \App\graph::whereIn('currencies', $values)
		  ->where('date','=',$history[$i])
		  ->get();	 
		  if(count($search))
		  {    
	          if(strcmp($fromcurrency.$tocurrency,$search[0]->currencies)==0)
			  $graphrates[$i]=$search[0]->result;
		      else
			  $graphrates[$i]=1/$search[0]->result;
		  }
		  
	 else
	 {
  $url="http://apilayer.net/api/historical?access_key=36f135772707a7c2b28a902c07369d50&date=".$history[$i]."&currencies=".$fromcurrency.",".$tocurrency."&format=1";
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $json = curl_exec($ch);
  curl_close($ch);
  $conversionResult = json_decode($json, true);
  $usdfrom=$conversionResult["quotes"]["USD".$fromcurrency];
  $usdto=$conversionResult["quotes"]["USD".$tocurrency];
  $graphrates[$i]=round($usdto/$usdfrom,5); 
  $add = new \App\graph;
  $add->currencies = $fromcurrency.$tocurrency;
  $add->result=$graphrates[$i];
  $add->date=$history[$i];
  $add->save();
	 }
	 }
	$graphrates[9]=$converted; 
$js_history = json_encode($history);
$js_graph = json_encode($graphrates);

if(count($_SESSION))
{
	$values = array($fromcurrency.$tocurrency, $tocurrency.$fromcurrency);
$search = \App\rate::whereIn('currencies', $values)->first();
 
$live_user = new \App\live_user;
if(strcmp($fromcurrency.$tocurrency,$search->currencies)==0)
 $live_user->reverse =0 ;
else
$live_user->reverse=1;	
$live_user->live_id = $search->id;
$live_user->user_id = $_SESSION["user"];
$live_user->date = date("Y-m-d");
$live_user->save();

}	
  
  
  echo '
  	<nav class="navbar navbar-inverse ">
    <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href='.url("/").'>CURR<span style="color:yellow;">&#8364</span>NCY Converter</a>
    </div>
	</div>
	</nav>
	<center>
  <div style="color:#fff;background-color:#337ab7;border-color:#2e6da4" id="quantity" >'.$quantity.'</div>
  <div style="color:#fff;background-color:#5bc0de;border-color:#46b8da" id="from" >'.$fromcurrency.'</div>
  <div style="color:#fff;background-color:#f0ad4e;border-color:#eea236" id="eq">=</div>
  <div style="color:#fff;background-color:#5cb85c;border-color:#4cae4c" id="result" >'.$converted.'</div>
  <div style="color:#fff;background-color:#5bc0de;border-color:#46b8da" id="to" >'.$tocurrency.'</div>
   </center>
  <center><div style="width:30%">
 
				<div style:"position:absolute;top:40%;" >
				<canvas id="canvas"  height="450" width="600"></canvas>
			</div>
		</div>
		 </center>
	<script>
		var lineChartData = {
			labels :'.$js_history.',
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data :'.$js_graph.'
				}
				
			]
		}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myLine = new Chart(ctx).Line(lineChartData, {
			responsive: true
		});
	}
	</script>
  ';
  ?>;
  </body>
  </html>