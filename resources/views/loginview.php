<?php
session_start();
?>

<!DOCTYPE html>
	  <html>
	  <head>
	  <title>LOGGED IN</title>
	  <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="boot.css">
	  <script src="jquery.js"></script>
      <script src="boot.js"></script>
 </head>
 <body>
 <?php 
if(count($_SESSION))
{
  $id =$_SESSION["user"];
  $account = \App\account::find($id);
  $signin = $account->name;
  $signinurl = url("loginview");
  $login= "logout";
  $loginurl = url("logout");
}

else
{
	$signin = "sign up";
  $signinurl = url("signup");
  $login= "login";
  $loginurl = url("login");
}

$user_id = $_SESSION["user"];
echo ' 
	  <nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href='.url("/").'>CURR<span style="color:yellow;">&#8364</span>NCY Converter</a>
    </div>
	<div class="collapse navbar-collapse" id="myNavbar">
	<ul class="nav navbar-nav">
        <li class="active"><a data-toggle="tab" href="#live">Live searches made</a></li>
        <li><a data-toggle="tab" href="#history">Historical searches made</a></li>
    </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href='.$signinurl.'><span class="glyphicon glyphicon-user"></span>'.$signin.'</a></li>
        <li><a href='.$loginurl.'><span class="glyphicon glyphicon-log-in"></span>'.$login.'</a></li>
      </ul>
    </div>
  </div>
</nav>
 <div class="tab-content">
    <div id="live" class="tab-pane fade in active">
      <center><h1>Live searches made</h1></center>
';

$live_user = \App\live_user::where("user_id","=",$user_id)->get();
if(count($live_user))
{
	foreach($live_user as $search)
	{    
		$live = \App\rate::find($search->live_id);
		if($search->reverse==0)
		echo '<center><div class="btn btn-primary btn-sm">'.$live->currencies[0].$live->currencies[1].$live->currencies[2].'  to  '.$live->currencies[3].$live->currencies[4].$live->currencies[5].'   on   '.$search->date.'</div><center><br>';
        else
	    echo '<center><div class="btn btn-primary btn-sm">'.$live->currencies[3].$live->currencies[4].$live->currencies[5].'  to  '.$live->currencies[0].$live->currencies[1].$live->currencies[2].'   on   '.$search->date.'</div><center><br>';
			
	}
}
else
{
	echo '<center><div class="btn btn-danger btn-sm">NONE</div><center><br>';
}

echo '
</div>
<div id="history" class="tab-pane fade">
     <center> <h1>Historical searches made</h1></center> 
';
$historical_user = \App\historical_user::where("user_id","=",$user_id)->get();
if(count($historical_user ))
{
	foreach($historical_user  as $search)
	{
		$live = \App\graph::find($search->history_id);
		if($search->reverse==0)
		echo '<center><div class="btn btn-primary btn-sm">'.$live->currencies[0].$live->currencies[1].$live->currencies[2].'  to  '.$live->currencies[3].$live->currencies[4].$live->currencies[5].'   as of   '.$search->date.'</div><center><br>';
        else
	    echo '<center><div class="btn btn-primary btn-sm">'.$live->currencies[3].$live->currencies[4].$live->currencies[5].'  to  '.$live->currencies[0].$live->currencies[1].$live->currencies[2].'   as of   '.$search->date.'</div><center><br>';
	}
}
else
{
	echo '<center><div class="btn btn-danger btn-sm">NONE</div><center><br>';
}

echo '</div>
<div>';
 ?>
 </body>
 </html>