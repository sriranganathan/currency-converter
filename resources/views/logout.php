<?php
session_start();
?>

<!DOCTYPE html>
	  <html>
	  <head>
	  <title>LOGGED OUT</title>
	  <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="boot.css">
	  <script src="jquery.js"></script>
      <script src="boot.js"></script>
 </head>
 <body>
 <?php 
session_unset(); 
 	 echo '
         <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <div class="alert alert-success col-sm-4 col-xs-8 text-center" >LOG-OUT Successful</div>
		 </div>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("login").' class="btn btn-info col-sm-4 col-xs-8 btn-lg">click here to login again</a>
		 </div>
		 <br>
		 <div class="row">
		 <div class="col-sm-4 col-xs-2 " ></div>
		 <a href='.url("/").' class="btn btn-info col-sm-4 col-xs-8 btn-lg "> click here to use to the app without an account</a>
		 </div>';
 ?>
 </body>
 </html>